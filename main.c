#include <msp430.h>
#include "lib/timer.h"
#include "lib/rs485.h"
#include "lib/rotate.h"
#include "lib/interface.h"

timer_control_t tcp;
rs485_control_t rcp;
rotate_control_t rocp;
interface_control_t icp;

int main(void)
{

    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    __bis_SR_register(SCG0);                 // disable FLL
    CSCTL3 |= SELREF__REFOCLK;               // Set REFO as FLL reference source
    CSCTL1 = DCOFTRIMEN_1 | DCOFTRIM0 | DCOFTRIM1 | DCORSEL_3;// DCOFTRIM=3, DCO Range = 8MHz
    CSCTL2 = FLLD_0 + 243;                  // DCODIV = 8MHz
    __delay_cycles(3);
    __bic_SR_register(SCG0);                // enable FLL

    PM5CTL0 &= ~LOCKLPM5;

    Timer_Init (&tcp);
    RS485_Init(&rcp, &tcp);
    Rotate_Init(&rocp, &rcp);
    Interface_Init(&icp, &rcp, &rocp);

    __bis_SR_register(GIE);

    while(1)
    {
        Timer_Task(&tcp);
        RS485_Task(&rcp);
        Rotate_Task(&rocp);
        Interface_Task(&icp);
    }
}

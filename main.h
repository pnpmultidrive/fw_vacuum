/*
 * main.h
 *
 *  Created on: May 4, 2023
 *      Author: camilodiaz
 */

#ifndef MAIN_H_
#define MAIN_H_

#define DEVICE_TYPE             'h'//Header type
#define DEVICE_MASTER           'm'//Master device type
#define DEVICE_RESPONSE         'r'//Response message

#define FIRMWARE_VERSION        "rhf1.07\n"
#define FIRMWARE_VERSION_LEN    8

#define DEVICE_HEADER_TYPE      "rhtv\n"
#define DEVICE_HEADER_TYPE_LEN  5

#endif /* MAIN_H_ */

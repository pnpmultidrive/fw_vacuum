/*
 * rotate.c
 *
 *  Created on: May 3, 2023
 *      Author: camilodiaz
 */

#include <msp430.h>
#include "lib/rotate.h"
#include "lib/rs485.h"
#include "main.h"

void Rotate_Init (rotate_control_t *rocp, rs485_control_t *rcp)
{
    ROTATE_PIN_IN1_DIR |= ROTATE_PIN_IN1;
    ROTATE_PIN_IN2_DIR |= ROTATE_PIN_IN2;
    ROTATE_PIN_IN3_DIR |= ROTATE_PIN_IN3;
    ROTATE_PIN_IN4_DIR |= ROTATE_PIN_IN4;

    ROTATE_PIN_IN1_OUT &= ~ROTATE_PIN_IN1;
    ROTATE_PIN_IN2_OUT &= ~ROTATE_PIN_IN2;
    ROTATE_PIN_IN3_OUT &= ~ROTATE_PIN_IN3;
    ROTATE_PIN_IN4_OUT &= ~ROTATE_PIN_IN4;

    TB1CCR0 = 20000;
    TB1CTL = TBSSEL__SMCLK | MC__UP;             // SMCLK, UP mode

    rocp->step_count = 1170;
    rocp->step = 1;//First step in stepper motor (1 to 5)
    rocp->step_dir = 1;//Rigth or Left
    rocp->state = ROTATE_STATE_TESTP;
    rocp->rcp = rcp;
    TB1CCTL0 |= CCIE;
}

void Rotate_Task (rotate_control_t *rocp)
{
    unsigned char buf[] = {DEVICE_RESPONSE,DEVICE_TYPE, '+' ,'0','\n'};//0->ok
    switch(rocp->state)
    {
    case ROTATE_STATE_TESTP:
        if(rocp->step_count == 0)
        {
            rocp->step_count = 1170;
            rocp->step_dir = -1;//Rigth or Left
            rocp->state = ROTATE_STATE_TESTN;
            TB1CCTL0 |= CCIE;
        }
        break;
    case ROTATE_STATE_TESTN:
        if(rocp->step_count == 0)
        {
            RS485_Print(rocp->rcp,DEVICE_HEADER_TYPE,DEVICE_HEADER_TYPE_LEN);
            rocp->state = ROTATE_STATE_IDLE;
        }
        break;
    case ROTATE_STATE_IDLE:
        break;
    case ROTATE_STATE_MOVING:
        if(rocp->step_count == 0)
        {
            ROTATE_PIN_IN1_OUT &= ~ROTATE_PIN_IN1;
            ROTATE_PIN_IN2_OUT &= ~ROTATE_PIN_IN2;
            ROTATE_PIN_IN3_OUT &= ~ROTATE_PIN_IN3;
            ROTATE_PIN_IN4_OUT &= ~ROTATE_PIN_IN4;
            rocp->state = ROTATE_STATE_RESPONSE;
        }
        break;
    case ROTATE_STATE_RESPONSE:
        if(RS485_Busy(rocp->rcp) == 0)
        {
            if(rocp->step_dir == 1)
                buf[2] = '+';
            else
                buf[2] = '-';
            RS485_Print(rocp->rcp,&buf[0],6);
            rocp->state = ROTATE_STATE_IDLE;
        }
        break;
    default:
        break;
    }
}

#pragma vector = TIMER1_B0_VECTOR
__interrupt void Timer1_B0_ISR(void)
{
    extern rotate_control_t rocp;

    rocp.step += rocp.step_dir;
    if(rocp.step == 0) rocp.step = 4;
    if(rocp.step == 5) rocp.step = 1;

    switch(rocp.step)
    {
    case 1:
        ROTATE_PIN_IN1_OUT |= ROTATE_PIN_IN1;
        ROTATE_PIN_IN2_OUT &= ~ROTATE_PIN_IN2;
        ROTATE_PIN_IN3_OUT &= ~ROTATE_PIN_IN3;
        ROTATE_PIN_IN4_OUT &= ~ROTATE_PIN_IN4;
        break;
    case 2:
        ROTATE_PIN_IN1_OUT &= ~ROTATE_PIN_IN1;
        ROTATE_PIN_IN2_OUT |= ROTATE_PIN_IN2;
        ROTATE_PIN_IN3_OUT &= ~ROTATE_PIN_IN3;
        ROTATE_PIN_IN4_OUT &= ~ROTATE_PIN_IN4;
        break;
    case 3:
        ROTATE_PIN_IN1_OUT &= ~ROTATE_PIN_IN1;
        ROTATE_PIN_IN2_OUT &= ~ROTATE_PIN_IN2;
        ROTATE_PIN_IN3_OUT |= ROTATE_PIN_IN3;
        ROTATE_PIN_IN4_OUT &= ~ROTATE_PIN_IN4;
        break;
    case 4:
        ROTATE_PIN_IN1_OUT &= ~ROTATE_PIN_IN1;
        ROTATE_PIN_IN2_OUT &= ~ROTATE_PIN_IN2;
        ROTATE_PIN_IN3_OUT &= ~ROTATE_PIN_IN3;
        ROTATE_PIN_IN4_OUT |= ROTATE_PIN_IN4;
        break;
    }

    rocp.step_count--;
    if(rocp.step_count == 0)
        TB1CCTL0 &=~CCIE;
}

//----------------------------------------------------------------------------
//-------------------------------- Interfaces --------------------------------
//----------------------------------------------------------------------------

void Rotate_Move(rotate_control_t *rocp, unsigned int grades, int dir)
{
    rocp->step_count = grades * ROTATE_STEPXGRADE;
    rocp->step_dir = dir;
    rocp->state = ROTATE_STATE_MOVING;
    TB1CCTL0 |= CCIE;
}

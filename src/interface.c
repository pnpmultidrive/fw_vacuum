/*
 * interface.c
 *
 *  Created on: May 4, 2023
 *      Author: camilodiaz
 */

#include <msp430.h>
#include "lib/interface.h"
#include "lib/rs485.h"
#include "lib/rotate.h"
#include "main.h"

void Interface_Init (interface_control_t *icp, rs485_control_t *rcp, rotate_control_t *rocp)
{
    icp->rcp = rcp;
    icp->rocp = rocp;

    P1DIR |= BIT4;
    P1OUT &= ~BIT4;
}

void Interface_Task (interface_control_t *icp)
{
    unsigned char aux, buf[32];
    unsigned int grades;
    unsigned char msg[] = {DEVICE_RESPONSE, DEVICE_TYPE ,'v','0','\n'};//0->ok

    aux = RS485_Available(icp->rcp);
    if(aux > 0)
    {
        RS485_Read(icp->rcp, &buf[0]);
        __no_operation();
        if(buf[0] == DEVICE_TYPE)
        {
            msg[2] = buf[1];
            if(buf[1] == 'f')//Firmware Version
            {
                RS485_Print(icp->rcp,FIRMWARE_VERSION,FIRMWARE_VERSION_LEN);
            }
            else if(buf[1] == 't')//Device type
            {
                RS485_Print(icp->rcp,DEVICE_HEADER_TYPE,DEVICE_HEADER_TYPE_LEN);
            }
            else if(buf[1] == 'i')//Init Vacumm
            {
                P1OUT |= BIT4;
                msg[3] = '0';
                RS485_Print(icp->rcp,&msg[0],6);
            }
            else if(buf[1] == 's')//Stop Vacumm
            {
                P1OUT &= ~BIT4;
                msg[3] = '0';
                RS485_Print(icp->rcp,&msg[0],6);
            }
            else if((buf[2]>= '0' && buf[2] <= '9') &&
                    (buf[3]>= '0' && buf[3] <= '9') &&
                    (buf[4]>= '0' && buf[4] <= '9'))
            {
                grades = (buf[2]&0x0F)*100;
                grades +=(buf[3]&0x0F)*10;
                grades +=(buf[4]&0x0F);
                if(buf[1] == '-')
                    Rotate_Move(icp->rocp,grades, -1);
                else if(buf[1] == '+')
                    Rotate_Move(icp->rocp,grades, 1);
                else
                {
                    msg[3] = '1';
                    RS485_Print(icp->rcp,&msg[0],6);
                }
            }
            else
            {
                msg[3] = '1';
                RS485_Print(icp->rcp,&msg[0],6);
            }

        }
    }
}

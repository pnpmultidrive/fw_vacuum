/*
 * timer.c
 *
 *  Created on: May 3, 2023
 *      Author: camilodiaz
 */


#include <msp430.h>
#include "lib/timer.h"

void Timer_Init (timer_control_t *tcp)
{

    TB0CCTL0 |= CCIE;                             // TBCCR0 interrupt enabled
    TB0CCR0 = 8000;
    TB0CTL = TBSSEL__SMCLK | MC__UP;             // SMCLK, UP mode
}


void Timer_Task (timer_control_t *tcp)
{
    if(tcp->tick>0)
    {
        tcp->tick--;
        if(tcp->flags&TIMER_B_ENABLE != 0)
        {
            tcp->count++;
            if(tcp->count == tcp->limit)
            {
                tcp->flags |= TIMER_B_FINISH;
                tcp->count = 0;
                tcp->flags &= ~TIMER_B_ENABLE;
            }
        }
    }
}

#pragma vector = TIMER0_B0_VECTOR
__interrupt void Timer0_B0_ISR (void)
{
    extern timer_control_t tcp;
    tcp.tick++;
}


void Timer_Start(timer_control_t *tcp, unsigned int time)
{
    tcp->limit = time;
    tcp->count = 0;
    tcp->flags = TIMER_B_ENABLE;
}

unsigned char Timer_Available(timer_control_t *tcp)
{
    if((tcp->flags&TIMER_B_FINISH) == 0)
        return 0;
    tcp->flags &= ~TIMER_B_FINISH;
    return 1;
}

void Timer_Stop(timer_control_t *tcp)
{
    tcp->flags = 0;
    tcp->count = 0;
}

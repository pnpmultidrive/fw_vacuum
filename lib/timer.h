/*
 * timer.h
 *
 *  Created on: May 3, 2023
 *      Author: camilodiaz
 */

#ifndef LIB_TIMER_H_
#define LIB_TIMER_H_

#define TIMER_SIZE 1

#define TIMER_RS485    0

#define TIMER_B_ENABLE      0x01
#define TIMER_B_FINISH      0x02
#define TIMER_B_CONTINOUS   0x04

#define Timer_Restart(t) (t->count=0)

typedef struct timer_control_t timer_control_t;
struct timer_control_t
{
    unsigned char flags;
    unsigned int count;
    unsigned int limit;

    unsigned char tick;
};

void Timer_Init (timer_control_t *tcp);
void Timer_Task (timer_control_t *tcp);

void Timer_Start(timer_control_t *tcp, unsigned int time);
unsigned char Timer_Available(timer_control_t *tcp);
void Timer_Stop(timer_control_t *tcp);

#endif /* LIB_TIMER_H_ */

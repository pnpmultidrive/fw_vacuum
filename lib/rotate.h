/*
 * rotate.h
 *
 *  Created on: May 3, 2023
 *      Author: camilodiaz
 */

#ifndef LIB_ROTATE_H_
#define LIB_ROTATE_H_

#include "lib/rs485.h"

//----------------------------------------------------------------------------
//--------------------------------- Defines ----------------------------------
//----------------------------------------------------------------------------

#define ROTATE_STATE_TESTP      0
#define ROTATE_STATE_TESTN      1
#define ROTATE_STATE_IDLE       2
#define ROTATE_STATE_MOVING     3
#define ROTATE_STATE_RESPONSE   4


#define ROTATE_BUFFER_SIZE      4
#define ROTATE_STEPXGRADE       13

//----------------- Rotate Driver -----------------
#define ROTATE_PIN_IN1       BIT1
#define ROTATE_PIN_IN1_OUT   P2OUT
#define ROTATE_PIN_IN1_DIR   P2DIR
#define ROTATE_PIN_IN2       BIT6
#define ROTATE_PIN_IN2_OUT   P2OUT
#define ROTATE_PIN_IN2_DIR   P2DIR
#define ROTATE_PIN_IN3       BIT7
#define ROTATE_PIN_IN3_OUT   P2OUT
#define ROTATE_PIN_IN3_DIR   P2DIR
#define ROTATE_PIN_IN4       BIT5
#define ROTATE_PIN_IN4_OUT   P1OUT
#define ROTATE_PIN_IN4_DIR   P1DIR

//----------------------------------------------------------------------------
//--------------------------------- Structure --------------------------------
//----------------------------------------------------------------------------

typedef struct rotate_buffer_t rotate_buffer_t;
struct rotate_buffer_t
{
    unsigned int step;
    signed int dir;
};

typedef struct rotate_control_t rotate_control_t;
struct rotate_control_t
{
    unsigned char state;

    unsigned char step;
    unsigned int step_count;
    int step_dir;

    rs485_control_t *rcp;
};

//----------------------------------------------------------------------------
//---------------------------------- General ---------------------------------
//----------------------------------------------------------------------------

void Rotate_Init (rotate_control_t *rocp, rs485_control_t *rcp);
void Rotate_Task (rotate_control_t *rocp);

//----------------------------------------------------------------------------
//-------------------------------- Interfaces --------------------------------
//----------------------------------------------------------------------------

void Rotate_Move(rotate_control_t *rocp, unsigned int grades, int dir);

#endif /* LIB_ROTATE_H_ */
